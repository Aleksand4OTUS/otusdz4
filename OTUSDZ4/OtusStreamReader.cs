﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
/*
+ OtusStreamReader<T>
+ В конструкторе получаем Stream и ISerializer<T>. 
+ В методе GetEnumerator() вызываем serializer.Deserialize<T[]>(Stream) 
+ и проходимся в цикле по массиву, возвращая элементы с помощью yield return.

Да, можно было бы просто вернуть массив, но мы заодно научимся работать с yield.
*/

namespace OTUSDZ4
{
    public class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly Stream Stream;
        private readonly ISerializer<T> Serializer;
        private IEnumerable<T> data;

        // В конструкторе получаем Stream и ISerializer<T>
        public OtusStreamReader(Stream stream, ISerializer<T> serializer)
        {
            Stream = stream;
            Serializer = serializer;
        }

        public void Dispose()
        {
            Stream.Close();
            GC.SuppressFinalize(this);
        }
        
        // В методе GetEnumerator() вызываем serializer.Deserialize<T[]>(Stream) 
        public IEnumerator<T> GetEnumerator()
        {
            if (Stream != null)
            {
                Stream.Seek(0, SeekOrigin.Begin);
                data = Serializer.Deserialize<T>(Stream);
                foreach (T item in data)
                    yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
