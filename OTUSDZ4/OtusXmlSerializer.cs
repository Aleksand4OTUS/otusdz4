﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace OTUSDZ4
{
    public class OtusXmlSerializer<T> : ISerializer<T>
    {
        private readonly XmlWriterSettings XmlWriterSettings;
        public OtusXmlSerializer()
        {
            XmlWriterSettings = new XmlWriterSettings { Indent = true };
        }
        public string Serialize<Tdata>(Tdata item)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
              .UseAutoFormatting()
              .UseOptimizedNamespaces()
              .EnableImplicitTyping(typeof(Tdata))
              .Create();

            return serializer.Serialize(XmlWriterSettings, item);
        }

        public Tdata[] Deserialize<Tdata>(Stream stream)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
               .UseAutoFormatting()
               .UseOptimizedNamespaces()
               .EnableImplicitTyping(typeof(Tdata[]), typeof(Tdata))
               .Type<Tdata[]>()
               .Name("List")
               .Create();
            return serializer.Deserialize<Tdata[]>(new XmlReaderSettings { IgnoreWhitespace = false }, stream);
        }

        public IEnumerable<Tdata> Deserialize<Tdata>(String xml)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
               .Create();
            return serializer.Deserialize<IEnumerable<Tdata>>(new XmlReaderSettings { IgnoreWhitespace = false }, xml);
        }


    }
}
