﻿using System.Collections.Generic;
using System.IO;

namespace OTUSDZ4
{
    public interface ISerializer<T>
    {
        string Serialize<Tdata>(Tdata item);
        Tdata[] Deserialize<Tdata>(Stream stream);
       
        IEnumerable<Tdata> Deserialize<Tdata>(string data);
    }
}
