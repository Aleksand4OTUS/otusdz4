﻿using System.Collections.Generic;
using System.Linq;


namespace OTUSDZ4
{
    public enum SortDirection
    {
        Ascending,
        Descending
    }
    public interface IAlgorithm<T>
    {
        IEnumerable<TItems> Sort<TItems>(IEnumerable<TItems> items, string fieldName, SortDirection sortDirection);
    }
    public class Sorter<T> : IAlgorithm<T>
    {
        IEnumerable<TItems> IAlgorithm<T>.Sort<TItems>(IEnumerable<TItems> items, string fieldName, SortDirection sortDirection)
        {
            if (sortDirection == SortDirection.Ascending)
                return items.OrderBy(x => x.GetType().GetProperty(fieldName).GetValue(x, null));
            else
                return items.OrderByDescending(x => x.GetType().GetProperty(fieldName).GetValue(x, null));

            //return from x in items
            //       orderby  x.GetType().GetProperty(fieldName).GetValue(x, null) 
            //       select x;

        }

    }
}
