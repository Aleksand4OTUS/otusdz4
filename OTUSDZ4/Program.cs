﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OTUSDZ4
{
// Создаём набор классов и их интерфейсов
// Задание 1:
// Реализация IEnumerable<T> на примере чтения списка элементов из xml, json ил сsv файла.
// В качестве десериализатора можно использовать https://github.com/ExtendedXmlSerializer/home или http://csvhelper.com/

// Здесь более пошагово на примере xml и некоего класса Person:
// https://pastebin.com/6FJZanYv


// Задание 2:
// Создать интерфейс IAlgorithm, добавить в него метод. Например, сортировка.Применить интерфейс к классу из первого задания.
// (Помните, что реализовав IEnumerable<T>, для вашего класса становятся доступны методы LINQ?)

// Задание 3:
// Реализуйте интерфейсы из https://gist.github.com/viktor-nikolaev/46e588fc1c52bbf03186597af23fcd61
// Напишите тесты IAccountService.AddAcount() с использованием Moq.

    class Program
    {
        static void Main()
        {
            // Fill person list
            List<Person> persons = new List<Person> { 
                new Person{Name = "Василий", Age = 39},
                new Person{Name = "Андрей", Age = 40},
                new Person{Name = "Федор", Age = 20},
                new Person{Name = "Анна", Age = 15},
                new Person{Name = "Иннокентий", Age = 10},
                new Person{Name = "Мария", Age = 5},
                new Person{Name = "Ольга", Age = 55}
            };

            // Serialization
            ISerializer<Person> personSerializer = new OtusXmlSerializer<Person>();
            string xmlData = personSerializer.Serialize(persons);
            Console.WriteLine(xmlData);

            Console.WriteLine();

            // Deserialization
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(xmlData));
            using (OtusStreamReader<Person> otusStreamReader = new OtusStreamReader<Person>(stream, personSerializer))
            {
                IAlgorithm<Person> sorter = new Sorter<Person>();
                IEnumerable<Person> personsSorted = sorter.Sort(otusStreamReader, "Age", SortDirection.Descending);

                foreach (Person person in personsSorted)
                    Console.WriteLine($"Name: {person.Name}, Age: {person.Age}");

            }
            //stream.Close();
            
            Console.ReadLine();
        }
    }
}
