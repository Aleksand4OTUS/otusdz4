﻿using System;
using System.Collections.Generic;


namespace OTUSDZ4_Account
{
    class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
    }

    // В классе реализаторе сохранять данные в какой-нибудь файл. Формат на ваше усмотрение - json, xml, csv, etc
    interface IRepository<T>
    {
        // когда реализуете этот метод, используйте yield (его можно использовать просто в методе, без создания отдельного класса)
        IEnumerable<T> GetAll();
        T GetOne(Func<T, bool> predicate);
        void Add(T item);
    }

    interface IAccountService
    {
        // В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
        // Если валидация прошла успешно, то добавлять аккаунт в репозиторий
        void AddAccount(Account account);
    }
}
