﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using OTUSDZ4;

namespace OTUSDZ4_Account
{
    public class Repository<T> : IRepository<T>
    {
        const string repositoryFile = "Repository.xml";
        private readonly string filePath;

        public Repository()
        {
            filePath = Path.Combine(Environment.CurrentDirectory, repositoryFile);
            if (!File.Exists(filePath))
            {
                //File.Create(filePath);
                FileStream fs = new FileStream(filePath, FileMode.Create);
                fs.Close();
            }

        }

        public void Add(T item)
        {
            IEnumerable<T> list = ReadFromFile();
            Save(list.Append<T>(item).ToList());
        }
        public IEnumerable<T> GetAll()
        {
            return ReadFromFile();
        }

        public T GetOne(Func<T, bool> predicate)
        {
            IEnumerable<T> _items = ReadFromFile();
            return _items.FirstOrDefault(predicate);
        }

        private IEnumerable<T> ReadFromFile()
        {
            ISerializer<Account> otusXmlSerializer = new OtusXmlSerializer<Account>();
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(File.ReadAllText(filePath)));
            if (stream.Length == 0)
            {
                return Enumerable.Empty<T>();
            }


            using OtusStreamReader<Account> otusStreamReader = new OtusStreamReader<Account>(stream, otusXmlSerializer);
            IAlgorithm<Account> sorter = new Sorter<Account>();
            IEnumerable<Account> personsSorted = sorter.Sort(otusStreamReader, "FirstName", SortDirection.Descending);

            foreach (Account person in personsSorted)
                Console.WriteLine($"Name: {person.FirstName}, Age: {person.LastName}");

            return otusXmlSerializer.Deserialize<T>(stream);
            // return Enumerable.Empty<T>();


        }
private void Save(IEnumerable<T> data)
        {
            ISerializer<T> serializer = new OtusXmlSerializer<T>();    
            File.WriteAllText(filePath, serializer.Serialize(data));
        }
    }
}