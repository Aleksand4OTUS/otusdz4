﻿using System;


namespace OTUSDZ4_Account
{
    class AccountService : IAccountService
    {
        // В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
        // Если валидация прошла успешно, то добавлять аккаунт в репозиторий
        private readonly IRepository<Account> repository;
        public AccountService(IRepository<Account> repo)
        {
            repository = repo;
        }
        public void AddAccount(Account account)
        {
            if (IsValid(account))
            {
                // Add
                repository.Add(account);
            }
            else
            {
                // Exception
                throw new InvalidAccountException();

            }
        }

        private bool IsValid(Account account)
        {   
            if (String.IsNullOrEmpty(account.FirstName))
                return false;

            if (String.IsNullOrEmpty(account.LastName))
                return false;

            var today = DateTime.Today;
            var age = today.Year - account.BirthDate.Year;

            if (account.BirthDate.Date > today.AddYears(-age)) age--;

            return (age > 18);

        }



        [Serializable]
        public class InvalidAccountException : Exception
        {
            
            public InvalidAccountException() //Account account)
                : base(String.Format("Invalid Account"))
            {
            }

        }


    }
}
